
from urllib.parse import urlencode
from collections import OrderedDict

class Params(OrderedDict):
    def __init__(self, query):
        query = sorted(query.items())
        super(Params, self).__init__(query)

    def __str__(self):
        return urlencode(self, safe='').lower()
