
from flatdict import FlatDict
import json
import os

DEFAULT_CONFIG = {
    "cache": {
        "path": "cache.sqlite",
        "ttl": 864000,
        "ttl_jitter": 864000
    },
    "providers": {
        "omdb": {
          # "api_key"
        },
        "tmdb": {
          # "api_key"
        }
    }
}

ENVIRON_MAP = {
    "MC_OMDB_APIKEY": "providers/omdb/api_key",
    "MC_TMDB_APIKEY": "providers/tmdb/api_key",
    "MC_CACHE_PATH": "cache/path",
    "MC_CACHE_TTL": "cache/ttl",
    "MC_CACHE_TTL_JITTER": "cache/ttl_jitter",
}


def load_config(path):
    config = FlatDict(DEFAULT_CONFIG, delimiter='/')
    try:
        with open(path, 'rb') as fp:
            data = json.load(fp)
        config.update(data)
    except:
        pass

    # Environment variable overrides config.json
    for env_name, config_key in ENVIRON_MAP.items():
        if os.environ.get(env_name) is not None:
            config[config_key] = os.environ.get(env_name)

    if config.get('providers/omdb/api_key') is None or \
            config.get('providers/tmdb/api_key') is None:
        raise Exception('Missing api key configuration')

    return config
