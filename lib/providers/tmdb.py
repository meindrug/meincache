import re

import tmdbsimple as tmdb


# matches "Joe Smith (creator)" -> "Joe Smith"
EXTRA_INFO_PATTERN = re.compile(' \(.+\)$')
RATING_NAME_TRANSLATION = {
    "Internet Movie Database": "imdb",
    "Rotten Tomatoes": "tomatoes",
    "Metacritic": "metacritic"
}
VALID_PARAMS = {
    'search': [
        'query',
        'year',
    ]
}

import sys

class TMDBProvider(object):
    name = "tmdb"

    def __init__(self, config):
        tmdb.API_KEY = config['api_key']

    async def get(self, params):
        if "tmdb_id" in params:
            movie = tmdb.Movies(int(params['tmdb_id']))
            ids = movie.external_ids()
            info = movie.info()
            info['imdb_id'] = ids['imdb_id']
        elif "imdb_id" in params:
            results = tmdb.Find(params['imdb_id']).info(external_source='imdb_id')
            movie = results['movie_results'][0]
            movie = tmdb.Movies(int(movie['id']))
            info = movie.info()
            info['imdb_id'] = params['imdb_id']
        info['provider'] = self.name
        return info

    async def search(self, params):
        search = tmdb.Search()
        results = search.movie(**params)
        results = results['results'][:10]

        # tmdb doesnt seem to prioritize exact title matches
        exact_match = []
        for result in results:
            title_keys = ['title', 'original_title']
            titles = [result[key].lower() for key in title_keys if key in result]
            if params['query'].lower() in titles:
                exact_match += [result]
        if len(exact_match) != 0:
            results = exact_match

        merged = []
        # Add imdb_id to first 10 results
        for movie in results:
            ids = tmdb.Movies(movie['id']).external_ids()
            movie['imdb_id'] = ids['imdb_id']
            merged += [movie]
        return {'results': merged, 'provider': self.name}

    def translate_params(self, endpoint, params):
        params = { k: v for k, v in params.items() if k in VALID_PARAMS[endpoint]}
        if 'imdb_id' in params:
            params['imdbid'] = params.pop('imdb_id')
        return params
