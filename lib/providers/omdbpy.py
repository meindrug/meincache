
import re

from omdb import OMDBClient


# matches "Joe Smith (creator)" -> "Joe Smith"
EXTRA_INFO_PATTERN = re.compile(' \([^\)]+\)')
RATING_NAME_TRANSLATION = {
    "Internet Movie Database": "imdb",
    "Rotten Tomatoes": "tomatoes",
    "Metacritic": "metacritic"
}
VALID_PARAMS = {
    'get': [
        'title',
        'year',
        'imdb_id',
        'media_type',
        'page',
    ],
    'search': [
        'query',
        'title',
        'year',
        'media_type',
    ]
}

class OMDBProvider(object):
    name = 'omdb'

    def __init__(self, config):
        self.api_key = config['api_key']
        self.client = OMDBClient(apikey=config['api_key'])

    async def get(self, params):
        params = self.translate_params('get', params)
        result = self.client.get(**params)

        if result == {}:
            return {'error': 'NotFound'}

        # translate ratings
        new_ratings = {}
        for rating in result.get('ratings', list()):
            translated_name = RATING_NAME_TRANSLATION.get(rating['source'])
            if translated_name is not None:
                new_ratings[translated_name] = rating['value']
        new_ratings['imdb'] = result['imdb_rating']
        result['ratings'] = new_ratings

        # turn fields with comma deliminated lists to lists
        for key in ['genre', 'director', 'writer', 'actors', 'language', 'country']:
            if key in result:
                cleaned = EXTRA_INFO_PATTERN.sub('', result[key])
                cleaned = [b.strip() for b in cleaned.split(',')]
                # sometimes there are duplicate names
                result[key] = list(set(cleaned))

        # remove some extra keys
        for key in ['dvd', 'website', 'response']:
            if key in result:
                del result[key]

        result['provider'] = self.name

        return result

    async def search(self, params):
        params = self.translate_params('search', params)
        query = params.pop('query')
        results = self.client.search(query, **params)
        return {
            "results": results,
            "provider": self.name,
        }

    def translate_params(self, endpoint, params):
        params = { k: v for k, v in params.items() if k in VALID_PARAMS[endpoint]}
        if 'imdb_id' in params:
            params['imdbid'] = params.pop('imdb_id')
        return params
