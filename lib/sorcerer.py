
import arrow

import asyncio
from collections import OrderedDict

from lib.providers.omdbpy import OMDBProvider
from lib.providers.tmdb import TMDBProvider

PROVIDERS = {
    'omdb': OMDBProvider,
    'tmdb': TMDBProvider,
}


class Sorcerer(object):
    def __init__(self, config):
        self.providers = OrderedDict()
        for name, cls in PROVIDERS.items():
            instance = cls(config.get('providers/' + name, {}))
            self.providers[name] = instance

    def choose_providers(self, endpoint, params):
        provider = params.pop('provider', None)
        if provider:
            return [self.providers[provider]]
        else:
            if endpoint == 'get':
                return [self.providers['omdb']]
            elif endpoint == 'search':
                return [self.providers['tmdb']]

    async def request(self, *jobs):
        return await asyncio.gather(*jobs)

    def get(self, params):
        providers = self.choose_providers('get', params)
        results = asyncio.run(self.request(*[p.get(params) for p in providers]))
        result = results[0]
        result['retrieved_at'] = arrow.utcnow().isoformat()
        return result

    def search(self, params):
        providers = self.choose_providers('search', params)
        results = asyncio.run(self.request(*[p.search(params) for p in providers]))
        result = results[0]
        result['retrieved_at'] = arrow.utcnow().isoformat()
        return result
