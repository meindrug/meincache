from datetime import datetime
import os
import random
import threading
import arrow
import time
from pony.orm import *

# import warnings
# from arrow.factory import ArrowParseWarning
# warnings.simplefilter("ignore", ArrowParseWarning)


db = Database()

class CachedEntry(db.Entity):
    retrieved_at = Required(datetime)
    expires_at = Required(datetime)
    endpoint = Required(str)
    params = Required(str)
    data = Required(Json)
    composite_key(endpoint, params)

MINUTES = 60
HOURS = 60 * 60
DAYS = 60 * 60 * 24

CLEANUP_INTERVAL = 1 * DAYS

ISOFORMAT = "YYYY-MM-DD[T]HH:mm:ss.SZZ"
PONYFORMAT = "YYYY-MM-DD HH:mm:ss.SZZ"

class CacheCleanupThread(threading.Thread):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.daemon = True

    def run(self, *args, **kwargs):
        next_time = time.time() + CLEANUP_INTERVAL
        while True:
            while time.time() < next_time:
                time.sleep(60)
            with db_session:
                now = arrow.utcnow().isoformat()
                delete(c for c in CachedEntry if c.expires_at < now)
            next_time = time.time() + CLEANUP_INTERVAL

def cleanup_cache():
    s = sched.scheduler(time.time, time.sleep)
    s.enter(3600, 1, print_time)



class Cache(object):
    def __init__(self, config, BASE_DIR):
        self.path = config.get('cache/path', 'cache.sqlite')
        if not self.path.startswith('/'):
            self.path = os.path.join(BASE_DIR, self.path)
        self.ttl = config.get('cache/ttl', 1 * DAYS)
        self.ttl_jitter = config.get('cache/ttl_jitter', 3 * DAYS)
        db.bind('sqlite', self.path, create_db=True)
        db.generate_mapping(create_tables=True)

        self.cleanup_thread = CacheCleanupThread()
        self.cleanup_thread.start()


    @db_session(optimistic=False)
    def lookup(self, endpoint, params):
        query = select(c for c in CachedEntry
                        if c.params == str(params) and c.endpoint == endpoint)
        result = query.first()
        if result is not None:
            if arrow.get(result.expires_at) > arrow.utcnow():
                return result.data
            else:
                result.delete()
                commit()
        return None

    @db_session
    def store(self, endpoint, params, response):
        select(c for c in CachedEntry
               if c.params == str(params) and c.endpoint == endpoint).delete(bulk=True)
        retrieved_at = arrow.get(response['retrieved_at'], ISOFORMAT)
        random_ttl = self.ttl + random.randrange(self.ttl_jitter)
        expires_at = retrieved_at.shift(seconds=random_ttl)

        entry = CachedEntry(retrieved_at=retrieved_at.datetime,
                            expires_at=expires_at.datetime, endpoint=endpoint,
                            params=str(params), data=response)
        commit()
