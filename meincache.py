from flask import Flask
from flask import request
import os
import sys

from lib.params import Params
from lib.config import load_config
from lib.sorcerer import Sorcerer
from lib.cache import Cache


BASE_DIR = os.path.dirname(os.path.realpath(__file__))

app = Flask(__name__)
application = app # for wsgi default

if len(sys.argv) > 1:
    config_path = sys.argv[1]
else:
    config_path = "config.json"

config = load_config('config.json')

cache = Cache(config, BASE_DIR)
sorcerer = Sorcerer(config)

@app.route('/search')
def search_view():
    params = Params(request.args)
    ignore_cache = params.pop('ignore_cache', 'false')
    params['media_type'] = 'movie'
    if ignore_cache == 'true':
        results = None # force a request
    else:
        results = cache.lookup('search', params)
    if not results:
        results = sorcerer.search(params)
        cache.store('search', params, results)
        results['cached'] = False
    else:
        results['cached'] = True
    return results

@app.route('/get')
def get_view():
    params = Params(request.args)
    ignore_cache = params.pop('ignore_cache', 'false')
    params['media_type'] = 'movie'
    if ignore_cache == 'true':
        result = None # force a request
    else:
        result = cache.lookup('get', params)
    if not result:
        result = sorcerer.get(params)
        cache.store('get', params, result)
        result['cached'] = False
    else:
        result['cached'] = True
    return result

if __name__ == '__main__':
    app.run(debug=True, port=5050)
