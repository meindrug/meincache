# Meinserver

A local caching proxy for media information lookups.
Meinserver standardizes media info data format for easy switching between sources.
Results are kept locally to speed up future requests.

Used for personal development purposes.

## Supported sources

- TheMovieDB (via tmdbsimple)
- The Open Movie Database (via omdbapi)

## Usage

/get
  imdb_id

/search
  query
  year


provider
ignore_cache

## Configuration

Either via json file:

```
{
  "cache": {
      "path": "cache.sqlite",
      "ttl": 864000,
      "ttl_jitter": 864000
  },
  "providers": {
    "omdb": {
      "api_key": "<YOUR KEY>"
    },
    "tmdb": {
      "api_key": "<YOUR KEY>"
    }
  }
}
```

or environment variables

OMDB_APIKEY
TMDB_APIKEY


## Limitations

- Currently search is hardcoded to TMDb and get is hardcoded to OMDb

## Disclaimer

- This is

## License

GNU General Public License v3.0  
(same as tmdbsimple)
