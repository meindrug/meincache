
# Official Python image
FROM python:3.8-alpine

RUN mkdir /meincache
WORKDIR /meincache
ADD Dockerfile Pipfile Pipfile.lock /meincache/

RUN apk add --no-cache python3-dev build-base linux-headers pcre-dev && \
    pip install pipenv uwsgi && \
    cd /meincache && \
    pipenv install --system --deploy && \
    apk del python3-dev build-base linux-headers pcre-dev

RUN apk add python3 pcre

ADD . /meincache

EXPOSE 5050

CMD uwsgi --http ':5050' --module meincache:app
